const ORG = Deno.env.get('ORG');
let
    page = 1,
    isDone = false;
do {
    const response = await fetch(`https://github.com/sponsors/${ORG}/sponsors_partial?page=${page++}`);
    if(response.status === 200){
        const text = await response.text();
        if(text.length)
            console.log([...(text).matchAll(/alt="@([^"]+)"/g)].map(_ => _[1]).join('\n'));
        else
            isDone = true;
    }
    else isDone = true;
}
while(!isDone);